package com.xin.common.kit;

public class RuningTime {
    //系统启动时间
    static long start = System.currentTimeMillis() / 1000;
    static final int secondsInMinute = 60;
    static final int secondsInHour = 60 * secondsInMinute;
    static final int secondsInDay = 24 * secondsInHour;

    public static String getRunningTime() {
        long end = System.currentTimeMillis() / 1000;
        int duration = (int) (end - start);
        int day = duration / secondsInDay;
        int r = duration % secondsInDay;
        int hour = r / secondsInHour;
        r = r % secondsInHour;
        int minute = r / secondsInMinute;
        int seconds = r % secondsInMinute;
        StringBuilder ret = new StringBuilder();
        if (day > 0) {
            ret.append(day).append("天");
        }
        if (hour > 0 || ret.length() > 0) {
            ret.append(hour).append("小时");
        }
        if (minute > 0 || ret.length() > 0) {
            ret.append(minute).append("分");
        }
        if (seconds > 0 || ret.length() > 0) {
            ret.append(seconds).append("秒");
        }
        return ret.toString();
    }
}
