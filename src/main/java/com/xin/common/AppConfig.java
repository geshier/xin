package com.xin.common;

import com.jfinal.config.*;
import com.jfinal.json.MixedJsonFactory;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;
import com.xin.auth.AdminAuthInterceptor;
import com.xin.common.kit.RuningTime;

public class AppConfig extends JFinalConfig {
    static Prop p;

    public static void loadConfig(){
        if (p==null){
            p= PropKit.useFirstFound("app-config-pro.txt","app-config-dev.txt");
        }
    }
    @Override
    public void configConstant(Constants me) {
        loadConfig();
        me.setDevMode(p.getBoolean("devMode",false));
        me.setJsonFactory(MixedJsonFactory.me());
        me.setToCglibProxyFactory();
        me.setInjectDependency(true);
        me.setInjectSuperClass(false);
    }

    @Override
    public void configRoute(Routes me) {
        me.add(new Routes() {
            @Override
            public void config() {
                this.addInterceptor(new AdminAuthInterceptor());
               // this.addInterceptor(new LayoutInterceptor());
                this.setBaseViewPath("/_view");
                this.scan("com.xin.");
            }
        });
    }

    @Override
    public void configEngine(Engine me) {
        me.setDevMode(p.getBoolean("devMode",false));
        me.setCompressorOn();
        me.addSharedObject("strKit",new StrKit());
        me.addSharedObject("RuningTime",new RuningTime());
    }

    @Override
    public void configPlugin(Plugins me) {

    }

    @Override
    public void configInterceptor(Interceptors me) {

    }

    @Override
    public void configHandler(Handlers me) {

    }

    public static DruidPlugin getDruidPlugin(){
        loadConfig();
        return new DruidPlugin(p.get("jdbcUrl"),p.get("user"),p.get("password").trim());
    }
}
